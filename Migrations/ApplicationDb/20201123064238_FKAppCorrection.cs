﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Recruitement_App___BEZAO_Test.Migrations.ApplicationDb
{
    public partial class FKAppCorrection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Candidates_CandidateId1",
                table: "Applications");

            migrationBuilder.DropIndex(
                name: "IX_Applications_CandidateId1",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "CandidateId1",
                table: "Applications");

            migrationBuilder.AlterColumn<string>(
                name: "CandidateId",
                table: "Applications",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_CandidateId",
                table: "Applications",
                column: "CandidateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Candidates_CandidateId",
                table: "Applications",
                column: "CandidateId",
                principalTable: "Candidates",
                principalColumn: "CandidateId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Candidates_CandidateId",
                table: "Applications");

            migrationBuilder.DropIndex(
                name: "IX_Applications_CandidateId",
                table: "Applications");

            migrationBuilder.AlterColumn<int>(
                name: "CandidateId",
                table: "Applications",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CandidateId1",
                table: "Applications",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Applications_CandidateId1",
                table: "Applications",
                column: "CandidateId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Candidates_CandidateId1",
                table: "Applications",
                column: "CandidateId1",
                principalTable: "Candidates",
                principalColumn: "CandidateId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
