﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Recruitement_App___BEZAO_Test.Migrations.ApplicationDb
{
    public partial class nullableFileId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Candidates_Files_PhotoId",
                table: "Candidates");

            migrationBuilder.AlterColumn<int>(
                name: "PhotoId",
                table: "Candidates",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Candidates_Files_PhotoId",
                table: "Candidates",
                column: "PhotoId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Candidates_Files_PhotoId",
                table: "Candidates");

            migrationBuilder.AlterColumn<int>(
                name: "PhotoId",
                table: "Candidates",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Candidates_Files_PhotoId",
                table: "Candidates",
                column: "PhotoId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
