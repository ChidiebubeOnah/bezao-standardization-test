﻿using Recruitement_App___BEZAO_Test.Interfaces;

namespace Recruitement_App___BEZAO_Test.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public ICandidateRepository CandidateRepository { get; }
        public IEmployerRepository EmployerRepository { get; }
        public IIndustryRepository IndustryRepository { get; }
        public IGenderRepository GenderRepository { get; }
        public IEducationLevelRepository EducationLevelRepository { get; }
        public IJobTypeRepository JobTypeRepository { get; }
        public IJobRepository JobRepository { get; }
        public IAppRepository AppRepository { get; }

        public UnitOfWork(
            ICandidateRepository candidateRepository,
            IEmployerRepository employerRepository,
            IIndustryRepository industryRepository,
            IGenderRepository genderRepository,
            IEducationLevelRepository educationLevelRepository,
            IJobTypeRepository jobTypeRepository,
            IJobRepository jobRepository,
            IAppRepository appRepository
        )
        {
            CandidateRepository = candidateRepository;
            EmployerRepository = employerRepository;
            IndustryRepository = industryRepository;
            GenderRepository = genderRepository;
            EducationLevelRepository = educationLevelRepository;
            JobTypeRepository = jobTypeRepository;
            JobRepository = jobRepository;
            AppRepository = appRepository;
        }
    }
}