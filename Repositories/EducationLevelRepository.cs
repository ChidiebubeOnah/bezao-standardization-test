﻿using System.Collections.Generic;
using System.Linq;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;

namespace Recruitement_App___BEZAO_Test.Repositories
{
    public class EducationLevelRepository : IEducationLevelRepository
    {
        private readonly ApplicationDbContext _context;

        public EducationLevelRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<EducationLevel> GetEducationLevels()
        {
            return _context.EducationLevels.ToList();
        }
    }
}