﻿using System.Collections.Generic;
using System.Linq;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;

namespace Recruitement_App___BEZAO_Test.Repositories
{
    public class GenderRepository : IGenderRepository
    {
        private readonly ApplicationDbContext _context;

        public GenderRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Gender> GetGenders()
        {
            return _context.Genders.ToList();
        }
    }
}