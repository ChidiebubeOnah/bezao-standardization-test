﻿using Microsoft.AspNetCore.Mvc;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Components
{
    public class Categories : ViewComponent
    {
        private readonly IJobRepository _job;

        public Categories(IJobRepository job)
        {
            _job = job;
        }
        public IViewComponentResult Invoke()
        {
            var model = _job.GetIndustries();

            return View(model);
        }
    }
}