﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Components
{
    public class Counters : ViewComponent
    {
        private readonly IJobRepository _job;
        private readonly IAppRepository _app;
        private readonly ICandidateRepository _candidate;
        private readonly IEmployerRepository _employer;

        public Counters(IJobRepository job, IAppRepository app, ICandidateRepository candidate , IEmployerRepository employer)
        {
            _job = job;
            _app = app;
            _candidate = candidate;
            _employer = employer;
        }
        public IViewComponentResult Invoke()
        {
            var model = new CountersViewModel
            {
                Jobs = _job.GetJobs().Count(),
                Candidates = _candidate.GetCandidates().Count(),
                Companies = _employer.GetEmployers().Count(),
                Applications = _app.GetApplications().Count()
            };

            return View(model);
        }
    }
}