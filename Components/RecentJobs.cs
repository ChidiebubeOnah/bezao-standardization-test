﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Components
{
    public class RecentJobs : ViewComponent
    {
        private readonly IJobRepository _jobRepository;

        public RecentJobs(IJobRepository jobRepository)
        {
            _jobRepository = jobRepository;
        }
            public IViewComponentResult Invoke()
            {
                var model = _jobRepository
                    .GetJobs()
                    .OrderByDescending(j => j.Id)
                    .Take(8)
                    .ToList();
                
                return View(model);
            }
        }
}

