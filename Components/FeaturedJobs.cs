﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Components
{
    public class FeaturedJobs : ViewComponent
    {
        private readonly IJobRepository _jobRepository;

        public FeaturedJobs(IJobRepository jobRepository)
        {
            _jobRepository = jobRepository;
        }
        public IViewComponentResult Invoke()
        {
            var model = _jobRepository
                .GetJobs()
                .Where(j => j.Feature)
                .OrderByDescending(j => j.Id)
                .Take(8)
                .ToList();

            return View("/Views/Shared/Components/RecentJobs/Default.cshtml",model);
        }
    }
}