﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Recruitement_App___BEZAO_Test.Interfaces;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;
using Recruitement_App___BEZAO_Test.Repositories;

namespace Recruitement_App___BEZAO_Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IJobRepository _jobRepo;

        public HomeController(ILogger<HomeController> logger, IJobRepository jobRepo)
        {
            _logger = logger;
            _jobRepo = jobRepo;
        }

        public IActionResult Index()
        {
            return View("Home");
        }

        public IActionResult JobOffers(string category, string query = null, string city=null)
        {
            var model = _jobRepo.GetJobs()
                    .Where(p =>
                        string.IsNullOrEmpty(category)
                        || p.Employer.Industry.Name
                            .ToLower().Equals(category.ToLower()))
                    .Where(v =>
                        string.IsNullOrEmpty(query) 
                        || v.Title.ToLower()
                            .Contains(query.ToLower()))
                    .Where(v => 
                        string.IsNullOrEmpty(city)
                        || v.Address.City.ToLower()
                            .Contains(city.ToLower())
                        || v.Address.State.ToLower()
                            .Contains(city.ToLower())
            );
            if (!String.IsNullOrWhiteSpace(category))
            {
                ViewBag.Category = category;
            }
            if (!String.IsNullOrWhiteSpace(query))
            {
                ViewBag.Query = query;
            }
            if (!String.IsNullOrWhiteSpace(city))
            {
                ViewBag.City = city;
            }
            return View(model);

           
        }
        public IActionResult JobOffer(int id)
        {
            var model = _jobRepo.GetJob(id);
            return View(model);


        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Info()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }
    }
}
