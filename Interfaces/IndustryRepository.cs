﻿using System.Collections.Generic;
using System.Linq;
using Recruitement_App___BEZAO_Test.Models;

namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public class IndustryRepository : IIndustryRepository
    {
        private readonly ApplicationDbContext _context;

        public IndustryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Industry> GetIndustries()
        {
            return _context.Industries.ToList();
        }
    }
}