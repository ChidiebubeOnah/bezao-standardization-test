﻿using System.Collections.Generic;

namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public interface IJobTypeRepository
    {
        IEnumerable<JobType> GetJobTypes();
    }
}