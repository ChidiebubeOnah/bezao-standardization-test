﻿using System.Collections.Generic;
using Recruitement_App___BEZAO_Test.Models;

namespace Recruitement_App___BEZAO_Test.Interfaces
{
    public interface IIndustryRepository
    {
        IEnumerable<Industry> GetIndustries();
    }
}