﻿using AutoMapper;
using Recruitement_App___BEZAO_Test.Models;
using Recruitement_App___BEZAO_Test.Models.ViewModels;

namespace Recruitement_App___BEZAO_Test.Profiles
{
    public class JobProfile : Profile
    {

        public JobProfile()
        {
            CreateMap<Job, JobViewModel>();
            CreateMap<JobViewModel, Job>();


        }
    }
}