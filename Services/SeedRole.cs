using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Recruitement_App___BEZAO_Test.Areas.Identity.Data;

namespace Recruitement_App___BEZAO_Test.Services
{
    public class SeedRole
    {

        private static RoleManager<IdentityRole> _roleManager;
    

        public static async void EnsurePopulated(IApplicationBuilder app)
        {
            var context = app.ApplicationServices
                .CreateScope().ServiceProvider
                .GetRequiredService<IdentityContext>();
            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }
            
            _roleManager = app.ApplicationServices
                .CreateScope().ServiceProvider
                .GetRequiredService<RoleManager<IdentityRole>>();

           await Create("Admin");
           await Create("Candidate");
           await Create("Employer");
        }

        
        public static async Task Create(string name)
        {
            if ( !await _roleManager.RoleExistsAsync(name))
            {
                var role = new IdentityRole {Name = name};
                await _roleManager.CreateAsync(role);
            }
        }
    }
}