using Recruitement_App___BEZAO_Test.Models;

public class JobType
{
    public int Id { get; set; }
    public string Type { get; set; }
    public Audit Audit { get; set; }
}