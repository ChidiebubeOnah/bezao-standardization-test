using System;
using System.Collections.Generic;

namespace Recruitement_App___BEZAO_Test.Models
{
    public class Job
    {
        public int Id { get; set; }

        public string EmployerId { get; set; }
        public Employer Employer { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Responsibility { get; set; }
        public string Background { get; set; }

        public int JobTypeId { get; set; }
        public JobType Type { get; set; }

        public double MinPay { get; set; }
        public double MaxPay { get; set; }

        public int MinYearExperience { get; set; }
        public int MaxYearExperience { get; set; }

        public Address Address { get; set; }
        public DateTime Deadline { get; set; }

        public Status Status { get; set; }
        public bool Feature { get; set; }


        public List<Application> Applications { get; set; }
        public Audit Audit { get; set; }
    }
}