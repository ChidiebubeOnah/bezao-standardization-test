namespace Recruitement_App___BEZAO_Test.Models
{
    public class File
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public FileType Type { get; set; }

        public Audit Audit { get; set; }



       
    }
}