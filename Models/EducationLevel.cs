using Recruitement_App___BEZAO_Test.Models;

public class EducationLevel
{
    public int Id { get; set; }
    public string Level { get; set; }
    public Audit Audit { get; set; }

}