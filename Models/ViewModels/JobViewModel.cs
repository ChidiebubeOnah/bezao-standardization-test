using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace Recruitement_App___BEZAO_Test.Models.ViewModels
{
    public class JobViewModel
    {

        [HiddenInput]
        public int? Id { get; set; }

        [HiddenInput]
        public string EmployerId { get; set; }
       

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string Responsibility { get; set; }

        [Required]
        public string Background { get; set; }

        [Required(ErrorMessage = "Select Job Type")]
        [Display(Name = "Job Type")]
        public int JobTypeId { get; set; }
        public IEnumerable<JobType> JobTypes { get; set; }

        [Required]
        [Range(20000.99, double.MaxValue)]
        public double MinPay { get; set; }

        public double? MaxPay { get; set; }

        [Required]
        public int MinYearExperience { get; set; }

        [Range(1, 50)]
        public int MaxYearExperience { get; set; }

        public Address Address { get; set; }

        [Required]
        [DataType(dataType:DataType.Date)]
        public DateTime? Deadline { get; set; }

        [Required]
        public Status Status { get; set; }
        public bool Feature { get; set; }


       

    }
}