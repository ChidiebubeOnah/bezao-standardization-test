using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace Recruitement_App___BEZAO_Test.Models.ViewModels
{
    public class AppReviewViewModel
    {


        [HiddenInput]
        [ValidateNever]
        public int AppId { get; set; }

        [HiddenInput]
        [ValidateNever]
        public string CandidateId { get; set; }

        [HiddenInput]
        public int JobId { get; set; }

        [Required]
        [Display(Name = "Fullname")]
        public string FullName { get; set; }

        [Display(Name = "Phone Number")]
        [Phone]
        public string PhoneNumber { get; set; }
        public File Resume { get; set; }

        [BindNever]
        public bool? Approve { get; set; }

        
    }
}