﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Recruitement_App___BEZAO_Test.Models.ViewModels
{
    public class EmployerProfileViewModel
    {

        public string EmployerId { get; set; }

        [BindNever]
        public string CompanyName { get; set; }

        [BindNever]
        public string Email { get; set; }

        [BindNever]
        public string UserName { get; set; }

        [BindNever]
        public int IndustryId { get; set; }
        public IEnumerable<Industry> Industries { get; set; }

        [Required(ErrorMessage = "Enter Date Founded")]
        [DataType(DataType.Date)]
        public DateTime? Founded { get; set; }

        [BindNever]
        public string PhoneNumber { get; set; }
        public string Bio { get; set; }

        public string Website { get; set; }

        [Required(ErrorMessage = "Residence cant be blank")]
        public Address Address { get; set; }


        [Required]
        [Range(20, int.MaxValue)]
        public int Capacity { get; set; }
        public File Logo { get; set; }

        [Display(Name = "Social Accounts")]
        public SocialAccount SocialAccount { get; set; }
    }
}