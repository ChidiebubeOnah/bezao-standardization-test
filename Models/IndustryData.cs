﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Recruitement_App___BEZAO_Test.Models
{
    public class IndustryData
    {
        public static List<Industry> Data()
        {
            
            return new List<Industry>()
            {
                new Industry
                {
                    Name = "Design, Art & Multimedia",
                    Icon = "flaticon-pen",
                    Audit = new Audit()
                },

                new Industry
                {
                    Name = "Education Training",
                    Icon = "flaticon-mortarboard",
                    Audit = new Audit()
                },
                new Industry
                {
                    Name = "Accounting / Finance",
                    Icon = "flaticon-bars",
                    Audit = new Audit()
                },
                new Industry
                {
                    Name = "Human Resource",
                    Icon = "flaticon-interview",
                    Audit = new Audit()
                },
                new Industry
                {
                    Name = "Telecommunications",
                    Icon = "flaticon-antenna",
                    Audit = new Audit()
                },
                new Industry
                {
                    Name = "Restaurant / Food Service",
                    Icon = "flaticon-food",
                    Audit = new Audit()
                },
                new Industry
                {
                    Name = "Construction / Facilities",
                    Icon = "flaticon-customer-support",
                    Audit = new Audit()
                },
                new Industry
                {
                    Name = "Health",
                    Icon = "flaticon-care",
                    Audit = new Audit()
                }
            };
        }
    }
}
